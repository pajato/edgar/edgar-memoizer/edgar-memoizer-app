plugins {
    val kotlinVersion = "1.6.21"
    kotlin("jvm") version kotlinVersion
    kotlin("plugin.serialization") version kotlinVersion
    id("edgar-plugin") version "0.9.1"
    id("maven-publish")
    id("org.jlleitschuh.gradle.ktlint") version "10.2.1"
    id("org.jetbrains.kotlinx.kover") version "0.4.2"
    id("io.github.gradle-nexus.publish-plugin") version "1.1.0"
    id("org.jetbrains.dokka") version kotlinVersion
    id("com.github.johnrengelman.shadow") version "7.1.2"
    signing
    application
}

val sourcesJar by tasks.creating(Jar::class) {
    archiveClassifier.set("sources")
    from(sourceSets.getByName("main").allSource)
}

val testJar by tasks.registering(Jar::class) {
    archiveClassifier.set("tests")
    from(sourceSets.test.get().output)
    exclude("**/*UnitTest*.class")
    exclude("**/*IntegrationTest.class")
    exclude(".lsp")
    exclude("sample_projects")
}

val javadocJar by tasks.creating(Jar::class) {
    archiveClassifier.set("javadoc")
    from("$buildDir/dokka/javadoc")
}
tasks.getByName("javadocJar").dependsOn("dokkaJavadoc")
tasks.getByName("build").dependsOn("dokkaJavadoc")

tasks.processTestResources {
    expand("version" to project.version)
}

group = Publish.GROUP
version = "0.9.4" // Use the new core that enhances the launch process.

val artifactVersion = version as String
val coreVersion = "0.9.6"

signing {
    isRequired = project.property("signing.keyId") != "NoSuchKey"
    if (isRequired) sign(publishing.publications)
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            artifactId = rootProject.name
            from(components["kotlin"])
            artifact(javadocJar)
            artifact(sourcesJar)
            artifact(testJar)
            artifact(tasks["shadowJar"])
            Publish.populatePom(pom)
        }
    }
}
tasks.getByName("build").dependsOn("shadowJar")
tasks.getByName("build").dependsOn("publishToMavenLocal")

object Publish {
    const val GROUP = "com.pajato.edgar"
    private const val POM_DESCRIPTION = "The Edgar Memoizer App project"
    private const val POM_DEVELOPER_ID = "pajatopmr"
    private const val POM_DEVELOPER_NAME = "Paul Michael Reilly"
    private const val POM_ORGANIZATION_NAME = "Pajato Technologies LLC"
    private const val POM_ORGANIZATION_URL = "https://gitlab.com/pajato"
    private const val POM_NAME = "edgar-memoizer-app"
    private const val POM_LICENSE_DIST = "repo"
    private const val POM_LICENSE_NAME = "The GNU Lesser General Public License, Version 3"
    private const val POM_LICENSE_URL = "https://www.gnu.org/copyleft/lesser.html"
    private const val POM_SCM_CONNECTION = "scm:git:https://gitlab.com/pajato/edgar/edgar-memoizer-app.git"
    private const val POM_SCM_DEV_CONNECTION = "scm:git:git@gitlab.com:pajato/edgar/edgar-memoizer-app.git"
    private const val POM_SCM_URL = "https://gitlab.com/pajato/edgar/edgar-memoizer-app"
    private const val POM_URL = "https://gitlab.com/pajato/edgar/edgar-memoizer-app"

    fun populatePom(pom: MavenPom) {
        fun populatePomBasic() = pom.apply {
            name.set(POM_NAME)
            description.set(POM_DESCRIPTION)
            url.set(POM_URL)
        }

        fun populatePomLicenses() = pom.apply {
            licenses {
                license {
                    name.set(POM_LICENSE_NAME)
                    url.set(POM_LICENSE_URL)
                    distribution.set(POM_LICENSE_DIST)
                }
            }
        }

        fun populatePomScm() = pom.apply {
            scm {
                url.set(POM_SCM_URL)
                connection.set(POM_SCM_CONNECTION)
                developerConnection.set(POM_SCM_DEV_CONNECTION)
            }
        }

        fun populatePomDevelopers() = pom.apply {
            fun populatePomDevelopers(spec: MavenPomDeveloperSpec) = pom.apply {
                fun populatePomDeveloper(dev: MavenPomDeveloper) = pom.apply {
                    fun populatePomOrganization(org: MavenPomOrganization) {
                        org.name.set(POM_ORGANIZATION_NAME)
                        org.url.set(POM_ORGANIZATION_URL)
                    }

                    dev.id.set(POM_DEVELOPER_ID)
                    dev.name.set(POM_DEVELOPER_NAME)
                    organization { populatePomOrganization(this) }
                }

                spec.developer { populatePomDeveloper(this) }
            }

            developers { populatePomDevelopers(this) }
        }

        populatePomBasic()
        populatePomLicenses()
        populatePomScm()
        populatePomDevelopers()
    }
}

nexusPublishing {
    repositories {
        sonatype {
            nexusUrl.set(uri("https://s01.oss.sonatype.org/service/local/"))
            snapshotRepositoryUrl.set(uri("https://s01.oss.sonatype.org/content/repositories/snapshots/"))
            username.set(project.property("SONATYPE_NEXUS_USERNAME") as String)
            password.set(project.property("SONATYPE_NEXUS_PASSWORD") as String)
        }
    }
}

ktlint {
    verbose.set(true)
    outputToConsole.set(true)
    coloredOutput.set(true)
    debug.set(false)
    android.set(false)
    outputColorName.set("RED")
    ignoreFailures.set(false)
    enableExperimentalRules.set(false)
    reporters {
        reporter(org.jlleitschuh.gradle.ktlint.reporter.ReporterType.CHECKSTYLE)
        reporter(org.jlleitschuh.gradle.ktlint.reporter.ReporterType.JSON)
        reporter(org.jlleitschuh.gradle.ktlint.reporter.ReporterType.HTML)
    }
    filter {
        exclude("**/style-violations.kt")
        exclude("**/generated/**")
        include("src/**/*.kt")
    }
}

kover {
    isEnabled = true
    coverageEngine.set(kotlinx.kover.api.CoverageEngine.INTELLIJ)
    intellijEngineVersion.set("1.0.622")
    generateReportOnCheck.set(true)
}

repositories {
    mavenCentral()
    mavenLocal()
}

dependencies {
    implementation(platform("org.jetbrains.kotlin:kotlin-bom"))
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core-jvm:1.6.1-native-mt")
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.3.3")
    implementation("com.pajato.edgar:edgar-logger:0.9.11")
    implementation("com.pajato.edgar:edgar-memoizer-core:$coreVersion")

    testImplementation(kotlin("test"))
    testImplementation("org.jetbrains.kotlinx:kotlinx-coroutines-test:1.6.1")
    testImplementation("com.pajato.edgar:edgar-memoizer-core:$coreVersion:tests")
}

tasks {
    compileKotlin { kotlinOptions { jvmTarget = "11" } }
    compileTestKotlin { kotlinOptions { jvmTarget = "11" } }
    test {
        useJUnitPlatform()
        extensions.configure(kotlinx.kover.api.KoverTaskExtension::class) {
            isEnabled = true
            binaryReportFile.set(file("$buildDir/custom/result.bin"))
            includes = listOf("""com.pajato..*""")
        }
        testLogging.showStandardStreams = true
    }
    koverVerify {
        rule {
            name = "Covered Executable Snippet Percentage"
            bound { minValue = 100 }
        }
    }
}

application {
    mainClass.set("MainKt")
}
