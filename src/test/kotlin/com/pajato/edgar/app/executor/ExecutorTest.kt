package com.pajato.edgar.app.executor

import com.pajato.edgar.memoizer.core.BaseTest
import com.pajato.edgar.memoizer.core.MemoizerCommand
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json

class ExecutorTest : BaseTest() {

    @Test fun `When running execute() with an invalid command name, verify diagnostic`() {
        val name = "noSuchCommand"
        val expected = "Unsupported command: $name"
        assertEquals(expected, execute(name))
    }

    @Test fun `When a nop command is collected, verify the diagnostic`() {
        val json = Json.encodeToString(MemoizerCommand("testNop()", "nop"))
        assertEquals("", getCollectorDiagnostic(listOf(json)))
    }

    @Test fun `When an exit command is collected, verify the diagnostic`() {
        val commandName = "exit"
        val json = Json.encodeToString(MemoizerCommand("testExit()", commandName))
        assertEquals(commandName, getCollectorDiagnostic(listOf(json)))
    }
}
