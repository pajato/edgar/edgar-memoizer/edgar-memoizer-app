package com.pajato.edgar.app.receiver

import com.pajato.edgar.app.executor.executeCommand
import com.pajato.edgar.app.executor.getCommandsAsByteArray
import com.pajato.edgar.logger.Log
import com.pajato.edgar.memoizer.core.AbstractFileChannel
import com.pajato.edgar.memoizer.core.BaseTest
import com.pajato.edgar.memoizer.core.Config
import com.pajato.edgar.memoizer.core.Config.commandsSendFile
import com.pajato.edgar.memoizer.core.LockFailFileChannel
import com.pajato.edgar.memoizer.core.MemoizerCommand
import com.pajato.edgar.memoizer.core.NullLockFileChannel
import com.pajato.edgar.memoizer.core.ThrowDiscriminant
import com.pajato.edgar.memoizer.core.getFakeException
import com.pajato.edgar.memoizer.core.sender.sendCommandToRunningApp
import com.pajato.edgar.memoizer.core.testConfigDir
import java.io.File
import java.nio.ByteBuffer
import java.nio.channels.FileChannel
import java.nio.channels.FileLock
import java.nio.file.Paths
import kotlin.io.path.exists
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertTrue
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class ReceiverTest : BaseTest() {

    @Test fun `When main with no arguments is invoked with a waiting exit command, verify no exception`() {
        sendCommandToRunningApp(MemoizerCommand("test()", "exit"), commandsSendFile.channel)
        main()
        assertEquals(Config.configBaseDir, testConfigDir)
    }

    @Test fun `When main with an argument is invoked with a waiting exit command, verify no exception`() {
        sendCommandToRunningApp(MemoizerCommand("test()", "exit"), commandsSendFile.channel)
        main(testConfigDir.absolutePath)
        assertEquals(Config.configBaseDir, testConfigDir)
    }

    @Test fun `When a nop command is executed, verify the diagnostic`() {
        val command = MemoizerCommand("testNop()", "nop")
        assertEquals("", executeCommand(command))
    }

    @Test fun `When an exit command is executed, verify the diagnostic`() {
        val command = MemoizerCommand("testExit()", "exit")
        assertEquals("exit", executeCommand(command))
    }

    @Test fun `When lock operation fails with a closed channel exception, verify the diagnostic`() {
        val channel = LockFailFileChannel(ThrowDiscriminant.ClosedChannel)
        assertFailsWith<LockFailChannelClosedException> { runAppMaybe(channel) }
    }

    @Test fun `When lock operation fails with an overlapping exception, verify the exception`() {
        val channel = LockFailFileChannel(ThrowDiscriminant.OverlappingFileLock)
        assertFailsWith<LockFailOverlappingException> { runAppMaybe(channel) }
    }

    @Test fun `When lock operation fails in runAppMaybe(), verify the exception`() {
        val channel = LockFailFileChannel(ThrowDiscriminant.IOError)
        assertFailsWith<LockFailIOErrorException> { runAppMaybe(channel) }
    }

    @Test fun `When lock operation fails with an unexpected exception, verify the exception`() {
        val channel = LockFailFileChannel(ThrowDiscriminant.Null)
        assertFailsWith<LockFailUnexpectedException> { runAppMaybe(channel) }
    }

    @Test fun `When the lock result is null, verify the diagnostic`() {
        val channel = NullLockFileChannel()
        assertFailsWith<LockFailAlreadyRunningException> { runAppMaybe(channel) }
    }

    @Test fun `When an exception occurs while reading a byte buffer, verify the result size`() {
        runBlocking {
            val result: ByteArray = getCommandsAsByteArray(ReadFailFileChannel(ThrowDiscriminant.IOError))
            assertEquals(0, result.size)
        }
    }

    @Test fun `When exceptions are simulated, verify the exit codes`() {
        assertEquals(1, getExitCode(LockFailAlreadyRunningException("testing...")))
        assertEquals(2, getExitCode(LockFailChannelClosedException()))
        assertEquals(3, getExitCode(LockFailIOErrorException()))
        assertEquals(4, getExitCode(LockFailOverlappingException()))
        assertEquals(5, getExitCode(LockFailUnexpectedException()))
        assertEquals(6, getExitCode(DirectoryArgumentIsFileException()))
        assertEquals(7, getExitCode(DirectoryArgumentCreateException("")))
        assertEquals(8, getExitCode(ExecuteFailedException("")))
        assertEquals(8, getExitCode(ExecuteFailedException(null)))
    }

    @Test fun `When calling main a second time, verify an already running lock fail exception`() {
        val channel: FileChannel = commandsSendFile.channel
        runBlocking {
            val job = launch {
                delay(1L)
                main()
                Log.add("Failed main() finished.")
                sendCommandToRunningApp(MemoizerCommand("test()", "exit"), channel)
            }
            main()
            job.join()
        }
    }

    @Test fun `When a lock is null because another Memoizer process is running, verify the exception`() {
        assertFailsWith<LockFailAlreadyRunningException> { runOrAbort(null) }
    }

    @Test fun `When creating an already created configuration directory, verify the exception`() {
        val file: File = File.createTempFile("foo", "txt").also { it.deleteOnExit() }
        assertFailsWith<DirectoryArgumentCreateException> { createDirectory(file.toPath()) }
    }

    @Test fun `When validating a non-directory, verify an InvalidDirectoryException`() {
        val file: File = File.createTempFile("foo", "txt").also { it.deleteOnExit() }
        assertFailsWith<DirectoryArgumentIsFileException> { validateArgument(file) }
    }

    @Test fun `When validating a non-existing directory, verify it gets created`() {
        val dir = Paths.get("${System.getProperty("user.dir")}/build/tmp/tempDir")
        validateArgument(dir.toFile())
        assertTrue(dir.exists())
    }
}

class ReadFailFileChannel(private val discriminant: ThrowDiscriminant) : AbstractFileChannel() {

    override fun read(dst: ByteBuffer?): Int = throw getFakeException(discriminant)

    override fun lock(position: Long, size: Long, shared: Boolean): FileLock { return TestFileLock() }

    override fun tryLock(position: Long, size: Long, shared: Boolean): FileLock { return TestFileLock() }

    inner class TestFileLock : FileLock(this, 0L, 0L, false) {
        override fun isValid(): Boolean { TODO("Not yet implemented") }

        override fun release() { }
    }
}
