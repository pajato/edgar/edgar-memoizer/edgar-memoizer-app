package com.pajato.edgar.app.executor

import com.pajato.edgar.app.receiver.ExecuteFailedException
import com.pajato.edgar.memoizer.core.Config
import com.pajato.edgar.memoizer.core.Diagnostic
import com.pajato.edgar.memoizer.core.MemoizerCommand
import java.nio.ByteBuffer
import java.nio.channels.FileChannel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.flow
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json

internal suspend fun executeSentCommands(): Diagnostic {
    var result: Diagnostic = ""

    fun getDiagnosticList(bytes: ByteArray) =
        getCollectorDiagnostic(String(bytes).split("\n").filter { it.isNotEmpty() })

    getCommandsAsByteArray().let { bytes -> flow { emit(bytes) }.collect { result = getDiagnosticList(bytes) } }
    return result
}

internal fun getCollectorDiagnostic(list: List<String>): Diagnostic =
    list.map { executeJsonCommand(it) }.filter { it == "exit" }.let { if (it.isEmpty()) "" else "exit" }

internal fun execute(name: String): Diagnostic = when (name) {
    "nop" -> ""
    "exit" -> name
    else -> "Unsupported command: $name"
}

internal fun executeCommand(command: MemoizerCommand) = execute(command.commandName)

internal fun executeJsonCommand(json: String): Diagnostic {
    val command: MemoizerCommand = Json.decodeFromString(json)
    return executeCommand(command)
}

internal suspend fun getCommandsAsByteArray(channel: FileChannel = Config.commandsReceiveFile.channel): ByteArray {
    val buffer: ByteBuffer = ByteBuffer.allocate(1024 * 1024)

    fun putBytesFromBuffer(bytes: ByteArray) {
        buffer.position(0)
        buffer.get(bytes, 0, bytes.size)
    }

    fun getBytes() = channel.read(buffer).let { count ->
        ByteArray(count).also { bytes ->
            putBytesFromBuffer(bytes)
            channel.truncate(0L)
        }
    }

    with(channel) {
        while (size() == 0L) delay(1L)
        val lock = try { lock() } catch (exc: Exception) { throw ExecuteFailedException(exc.message) }
        return try { getBytes() } catch (exc: Exception) { ByteArray(0) } finally { lock.release() }
    }
}
