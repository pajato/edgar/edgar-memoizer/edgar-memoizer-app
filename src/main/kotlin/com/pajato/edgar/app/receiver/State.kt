package com.pajato.edgar.app.receiver

internal enum class State : Pid {
    Running { override fun getPid(): Long = ProcessHandle.current().pid() },
    Exited { override fun getPid(): Long = -1L },
    Killed { override fun getPid(): Long = -2L },
}
