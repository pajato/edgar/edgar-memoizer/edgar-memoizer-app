package com.pajato.edgar.app.receiver

import com.pajato.edgar.logger.Log

internal fun exitWithCode(exc: AppException) { Log.add("$ExitMessage${getExitCode(exc)}") }

internal fun getExitCode(exc: AppException): Int = when (exc) {
    is LockFailAlreadyRunningException -> 1
    is LockFailChannelClosedException -> 2
    is LockFailIOErrorException -> 3
    is LockFailOverlappingException -> 4
    is LockFailUnexpectedException -> 5
    is DirectoryArgumentIsFileException -> 6
    is DirectoryArgumentCreateException -> 7
    is ExecuteFailedException -> 8
}
