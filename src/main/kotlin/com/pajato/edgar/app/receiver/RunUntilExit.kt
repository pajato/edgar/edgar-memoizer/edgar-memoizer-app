package com.pajato.edgar.app.receiver

import com.pajato.edgar.app.executor.executeSentCommands
import com.pajato.edgar.memoizer.core.Config
import java.nio.channels.FileLock
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

internal fun runUntilExitCommand(lock: FileLock) {
    savePid(State.Running.getPid())
    runUntilExit()
    savePid(State.Exited.getPid())
    lock.release()
}

internal fun runUntilExit(): Unit = runBlocking {
    launch(Dispatchers.IO) { do { val result = executeSentCommands() } while (result != "exit") }.join()
}

internal fun savePid(pid: Long) = with(Config.pidFile) {
    seek(0)
    writeLong(pid)
}
