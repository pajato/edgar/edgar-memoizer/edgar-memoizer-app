package com.pajato.edgar.app.receiver

import java.io.IOException
import java.nio.channels.ClosedChannelException
import java.nio.channels.FileChannel
import java.nio.channels.FileLock
import java.nio.channels.OverlappingFileLockException

internal fun runMemoizer(channel: FileChannel): Unit = runOrAbort(channel.getLock())

internal fun runOrAbort(lock: FileLock?): Unit =
    if (lock == null) throw LockFailAlreadyRunningException("Waiting!") else runUntilExitCommand(lock)

internal fun FileChannel.getLock(): FileLock? =
    try { tryLock() ?: throw LockFailAlreadyRunningException("Different process!") } catch (exc: Exception) {
        throwException(exc)
    }

internal fun throwException(exc: Exception): Nothing = when (exc) {
    is LockFailAlreadyRunningException -> throw exc
    is ClosedChannelException -> throw LockFailChannelClosedException()
    is OverlappingFileLockException -> throw LockFailOverlappingException()
    is IOException -> throw LockFailIOErrorException()
    else -> throw LockFailUnexpectedException()
}
